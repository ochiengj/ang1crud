import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAddComponent } from './component/user-add/user-add.component';
import { UserListComponent } from './component/user-list/user-list.component';

const routes: Routes = [
  {path:'users', component: UserListComponent},
  {path: 'adduser', component: UserAddComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
