import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';





@Injectable({
  providedIn: 'root'
})

export class UserService {


  private baseUrl: string;

  constructor(private http: HttpClient) { 
    this.baseUrl= 'http://localhost:8080/users';
  }


  
  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  public save(user: User) {
    return this.http.post<User>(this.baseUrl, user);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl)
  }





}
