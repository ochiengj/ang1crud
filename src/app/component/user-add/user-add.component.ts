import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent {

  useradd: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService ) {
      this.useradd = new User();
     }

 
     onSubmit() {
       this.userService.save(this.useradd).subscribe(result => this.gotoUserList());
     }

     gotoUserList() {
       this.router.navigate(['/users']);
     }

}
