import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users?: User[];

  enableEdit= false;
  enableEditIndex= null;

  constructor(

    private router: Router,
    private route: ActivatedRoute,    
    private userService: UserService) { }

  ngOnInit() {

    this.userService.findAll().subscribe(data => {
      this.users = data;
    });    

  }

  removeAllTutorials(): void {
    this.userService.deleteAll()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.gotoUserList();
        },
        error: (e) => console.error(e)
      });
  }
  gotoUserList() {
    this.router.navigate(['/users']);
  }

}
